/**Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки) */
let array = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
    ];

    function indexLine(a){
        let b = [];
        let c = 0;
        for( let i = 0; i<a.length; ++i){
            b[i] = 0;
            for (let u = 0; u<a.length; ++u){
              b[i] += a[i][u];
              if (c < b[i]){c = b[i]};
            };     
        };
        return console.log(b.indexOf(c));
    };
    indexLine(array);

let array2 = [
    [11, 12, 13],
    [21, 22, 23],
    [31, 32, 33]
];
    function indexCool(a){
        let b = a[0];
        let c = 0;
        for( let i = 1; i<a.length; ++i){
          for (let u = 0; u<a.length; ++u){
            b[u] += a[i][u];
            if(c < b[u]){c = b[u]};
          };
        };
        return console.log(b.indexOf(c));
    };
indexCool(array2);