/*Написать функцию, которая транспонирует матрицу */

function lessen_3_The_task_3(a){
    var b = [];
    for (i = 0; i<a.length; ++i){
        b[i]= [];
        for(u = 0; u<a.length; ++u){
            b[i][u] = a[u][i];
        };
    };
    return b;
};
var c = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
];
console.log(lessen_3_The_task_3(c));