/**Аналогично как в предыдущем задании этого урока отмечать фоновым цветом вывод статистики в 
 * разрезе каждого курса касательно средней оценки */
var student = [
    {name: 'Ivan', estimate: 4, course: 1, active: 1, email: 'random@email'},
    {name: 'luda', estimate: 3, course: 1, active: 1, email: 'random@email'},
    {name: 'inga', estimate: 2, course: 4, active: 0, email: 'random@email'},
    {name: 'Azizbek', estimate: 4, course: 5, active: 1, email: 'random@email'},
    {name: 'Alibec', estimate: 4, course: 4, active: 1, email: 'random@email'},
    {name: 'Taras', estimate: 3, course: 2, active: 1, email: 'random@email'},
    {name: 'Adolf', estimate: 5, course: 3, active: 1, email: 'random@email'},
    {name: 'Joseph', estimate: 4, course: 5, active: 1, email: 'random@email'},
    {name: 'Winston', estimate: 2, course: 3, active: 0, email: 'random@email'},
    {name: 'Ilya', estimate: 4, course: 1, active: 1, email: 'random@email'},
    {name: 'Sasha', estimate: 2, course: 5, active: 0, email:'random@email'},
    {name: 'Anita', estimate: 4, course: 2, active: 1, email: 'random@email'},
    {name: 'Diana', estimate: 1, course: 2, active: 0, email:'random@email'},
    {name: 'Joe', estimate: 4, course: 4, active: 1, email: 'random@email'},
    {name: 'Donald', estimate: 5, course: 1, active: 1, email: 'random@email'}
  ];

  function AverageGradePerCourse(student){
    let Statistics = [];
    function onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
    };

    let anchor = [];
    for (let i = 0; i < student.length; i++){
      anchor.push(
        student[i].course
      );
    };
    let anchor2 = anchor.filter(onlyUnique);

    anchor2.sort();
    for (let u = 0; u < anchor2.length; u++) {
      Statistics.push(
      {course: anchor2[u], average_rating: 0, no_active: 0, sumStudent:0 }
      );
    };

    for (let y = 0; y < student.length; y++) {
      for (let t = 0; t < Statistics.length; t++) {
        if (Statistics[t].course == student[y].course){
          Statistics[t].average_rating += student[y].estimate;
          Statistics[t].sumStudent += 1;
        };
        if (Statistics[t].course == student[y].course && student[y].active == 0){
          Statistics[t].no_active += 1; 
        };
      };
    };
    for (let r = 0; r < Statistics.length; r++) {
      Statistics[r].average_rating = Math.trunc(Statistics[r].average_rating / Statistics[r].sumStudent * 100) / 100;
    };
    
    let sumNo_active = 0;
    for (let e = 0; e < Statistics.length; e++) {
      sumNo_active += Statistics[e].no_active;
    };
    Statistics.push(
      {sumNo_active: sumNo_active}
      );
      return Statistics
};

let studentStatistics = AverageGradePerCourse(student);
console.log (studentStatistics);

function DisplayingStudentStatistics(studentStatistics){

    let tableStatistics = document.getElementById('Statistics');
    tableStatistics.innerHTML = ''
    
    for(let i = 0; i < studentStatistics.length; ++i){
      let tr = document.createElement('tr');
      tableStatistics.appendChild(tr);
    
      let tdCourse = document.createElement('td');
      tr.appendChild(tdCourse);
      tdCourse.innerHTML = studentStatistics[i].course;
    
      let tdEstimate = document.createElement('td');
      tr.appendChild(tdEstimate);
      tdEstimate.id = 'tdEstimate'
      tdEstimate.innerHTML = studentStatistics[i].average_rating;
      console.dir(tdEstimate)
    
      let inActivStudent = document.createElement('td');
      tr.appendChild(inActivStudent);
      inActivStudent.innerHTML = studentStatistics[i].no_active;

    
    if (i == studentStatistics.length){
      let trSumNo_active = document.createElement('td');
      tr.appendChild(trSumNo_active);
      trSumNo_active.innerHTML = studentStatistics.sumNo_active;
    };
  };
  
  let tdBackgroundColor = document.querySelectorAll('td');
    for (let u=0; u<tdBackgroundColor.length; u++) {
        if(tdBackgroundColor[u].textContent  <= 3 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FF0000"}
        else if (tdBackgroundColor[u].textContent  == 4 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FFFF00"}
        else if (tdBackgroundColor[u].textContent  == 5 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#008000"}
        console.dir(tdBackgroundColor[u])
    };

  };
  DisplayingStudentStatistics(studentStatistics);