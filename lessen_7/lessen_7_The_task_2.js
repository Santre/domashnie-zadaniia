/**В ряде предыдущих заданий - выделять красным цветом тех студентов которые имеют оценку 3 и менее. 
 * которые от 3 до 4  - желтым и которые 4 и выше - зеленым. */
var student = [
    {name: 'Ivan', estimate: 4, course: 1, active: 1, email: 'random@email'},
    {name: 'luda', estimate: 3, course: 1, active: 1, email: 'random@email'},
    {name: 'inga', estimate: 2, course: 4, active: 0, email: 'random@email'},
    {name: 'Azizbek', estimate: 4, course: 5, active: 1, email: 'random@email'},
    {name: 'Alibec', estimate: 4, course: 4, active: 1, email: 'random@email'},
    {name: 'Taras', estimate: 3, course: 2, active: 1, email: 'random@email'},
    {name: 'Adolf', estimate: 5, course: 3, active: 1, email: 'random@email'},
    {name: 'Joseph', estimate: 4, course: 5, active: 1, email: 'random@email'},
    {name: 'Winston', estimate: 2, course: 3, active: 0, email: 'random@email'},
    {name: 'Ilya', estimate: 4, course: 1, active: 1, email: 'random@email'},
    {name: 'Sasha', estimate: 2, course: 5, active: 0, email:'random@email'},
    {name: 'Anita', estimate: 4, course: 2, active: 1, email: 'random@email'},
    {name: 'Diana', estimate: 1, course: 2, active: 0, email:'random@email'},
    {name: 'Joe', estimate: 4, course: 4, active: 1, email: 'random@email'},
    {name: 'Donald', estimate: 5, course: 1, active: 1, email: 'random@email'}
  ];

  function FormationOf_a_ContingentOfStudents(student){
    let table = document.querySelector('table tbody');
    table.innerHTML = ''
    for(let i = 0; i < student.length; ++i){
    let tr = document.createElement('tr');
    table.appendChild(tr);

    let tdName = document.createElement('td');
    tr.appendChild(tdName);
    tdName.innerHTML = student[i].name;
    

    let tdCourse = document.createElement('td');
    tr.appendChild(tdCourse);
    tdCourse.innerHTML = student[i].course;

    let tdEstimate = document.createElement('td');
    tr.appendChild(tdEstimate);
    tdEstimate.innerHTML = student[i].estimate;

    let tdEmail = document.createElement('td');
    tr.appendChild(tdEmail);
    tdEmail.innerHTML = student[i].email;

    let tdDelete = document.createElement('td');
    tdDelete.innerHTML = '<button class="idTable">Удалить</button>';
    tr.appendChild(tdDelete);

    let tdBackgroundColor = document.querySelectorAll('td');
    for (let u=0; u<tdBackgroundColor.length; u++) {
        let num = tdBackgroundColor[u].textContent;
        if(num  <= 3 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FF0000"}
        else if (num  == 4 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FFFF00"}
        else if (num  == 5 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#008000"}
    };
  };
  };
  FormationOf_a_ContingentOfStudents(student);