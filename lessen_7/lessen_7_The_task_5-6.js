/**По нажатие на имя студента - удалять имя, вместо имени показывать форму ввода - по нажатию на 
ENTER сохранять новое имя для этого студента, удалять форму ввода и выводить в списке новое имя студента */
/**По аналогии предыдущего пункта сделать тоже самое с номером курса и с оценкой студента. 
 * Не забыть что при изменении оценки статистика также должна быть пересчитана и выведена новая статистика. */
var student = [
    {name: 'Ivan', estimate: 4, course: 1, active: 1, email: 'random@email'},
    {name: 'luda', estimate: 3, course: 1, active: 1, email: 'random@email'},
    {name: 'inga', estimate: 2, course: 4, active: 0, email: 'random@email'},
    {name: 'Azizbek', estimate: 4, course: 5, active: 1, email: 'random@email'},
    {name: 'Alibec', estimate: 4, course: 4, active: 1, email: 'random@email'},
    {name: 'Taras', estimate: 3, course: 2, active: 1, email: 'random@email'},
    {name: 'Adolf', estimate: 5, course: 3, active: 1, email: 'random@email'},
    {name: 'Joseph', estimate: 4, course: 5, active: 1, email: 'random@email'},
    {name: 'Winston', estimate: 2, course: 3, active: 0, email: 'random@email'},
    {name: 'Ilya', estimate: 4, course: 1, active: 1, email: 'random@email'},
    {name: 'Sasha', estimate: 2, course: 5, active: 0, email:'random@email'},
    {name: 'Anita', estimate: 4, course: 2, active: 1, email: 'random@email'},
    {name: 'Diana', estimate: 1, course: 2, active: 0, email:'random@email'},
    {name: 'Joe', estimate: 4, course: 4, active: 1, email: 'random@email'},
    {name: 'Donald', estimate: 5, course: 1, active: 1, email: 'random@email'}
  ];

  function FormationOf_a_ContingentOfStudents(student){
    let table = document.querySelector('table tbody');
    table.innerHTML = ''
    for(let i = 0; i < student.length; ++i){
    let tr = document.createElement('tr');
    table.appendChild(tr);

    let tdName = document.createElement('td');
    tdName.addEventListener('click',
      (function(index){
        return function(event){
          let inputNam = document.querySelectorAll('.neme');

          for (let u = 0; u <inputNam.length; ++u){
            let value = inputNam[u].value;
            let cell = inputNam[u].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputName = document.createElement('input');
          inputName.type = 'text';
          inputName.className = 'neme';
          inputName.value = student[index].name;
          tdName.innerHTML = '';
          tdName.appendChild(inputName);

          inputName.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let value = event.target.value
              let cell = event.target.closest('td');
              cell.innerHTML = '<span>' + value + '</span>';
              self.student[index].name = event.target.value;
              // self.update(self.student[index]);
            };
          });
          inputName.focus();
        }  
    })(i));
    tr.appendChild(tdName);
    tdName.innerHTML = student[i].name
    

    let tdCourse = document.createElement('td');
    tdCourse.addEventListener('click',
      (function(index){
        return function(event){
          let inputCourse = document.querySelectorAll('.course');

          for (let y = 0; y <inputCourse.length; ++y){
            let value = inputCourse[y].value;
            let cell = inputCourse[y].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputCours = document.createElement('input');
          inputCours.type = 'number';
          inputCours.className = 'course';
          inputCours.value = student[index].course;
          tdCourse.innerHTML = '';
          tdCourse.appendChild(inputCours);

          inputCours.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let value = event.target.value
              let cell = event.target.closest('td');
              cell.innerHTML = '<span>' + value + '</span>';
              self.student[index].course = event.target.value;
              // self.update(self.student[index]);
              DisplayingStudentStatistics(AverageGradePerCourse(student));
            };
          });
          inputCours.focus();
        }  
    })(i));
    tr.appendChild(tdCourse);
    tdCourse.innerHTML = student[i].course;

    let tdEstimate = document.createElement('td');
    tdEstimate.addEventListener('click',
      (function(index){
        return function(event){
          let inputEstimate = document.querySelectorAll('.estimate');

          for (let t = 0; t <inputEstimate.length; ++t){
            let value = inputEstimate[t].value;
            let cell = inputEstimate[t].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputEstimat = document.createElement('input');
          inputEstimat.type = 'number';
          inputEstimat.className = 'estimate';
          inputEstimat.value = student[index].estimate;
          tdEstimate.innerHTML = '';
          tdEstimate.appendChild(inputEstimat);

          inputEstimat.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let value = event.target.value
              let cell = event.target.closest('td');
              cell.innerHTML = '<span>' + value + '</span>';
              self.student[index].estimate = event.target.value;
              // self.update(self.student[index]);
              DisplayingStudentStatistics(AverageGradePerCourse(student));
            };
          });
          inputEstimat.focus();
        }  
    })(i));
    tr.appendChild(tdEstimate);
    tdEstimate.innerHTML = student[i].estimate;

    let tdEmail = document.createElement('td');
    tr.appendChild(tdEmail);
    tdEmail.innerHTML = student[i].email;

    let tdDelete = document.createElement('td');
    tdDelete.innerHTML = '<button class="idTable">Удалить</button>';
    tr.appendChild(tdDelete);

  };
  };
  FormationOf_a_ContingentOfStudents(student);

  function AverageGradePerCourse(student){
    let Statistics = [];
    function onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
    };

    let anchor = [];
    for (let i = 0; i < student.length; i++){
      anchor.push(
        student[i].course
      );
    };
    let anchor2 = anchor.filter(onlyUnique);

    anchor2.sort();
    for (let u = 0; u < anchor2.length; u++) {
      Statistics.push(
      {course: anchor2[u], average_rating: 0, no_active: 0, sumStudent:0 }
      );
    };

    for (let y = 0; y < student.length; y++) {
      for (let t = 0; t < Statistics.length; t++) {
        if (Statistics[t].course == student[y].course){
          Statistics[t].average_rating += student[y].estimate;
          Statistics[t].sumStudent += 1;
        };
        if (Statistics[t].course == student[y].course && student[y].active == 0){
          Statistics[t].no_active += 1; 
        };
      };
    };
    for (let r = 0; r < Statistics.length; r++) {
      Statistics[r].average_rating = Math.trunc(Statistics[r].average_rating / Statistics[r].sumStudent * 100) / 100;
    };
    
    let sumNo_active = 0;
    for (let e = 0; e < Statistics.length; e++) {
      sumNo_active += Statistics[e].no_active;
    };
    Statistics.push(
      {sumNo_active: sumNo_active}
      );
      return Statistics
};

let studentStatistics = AverageGradePerCourse(student)

function DisplayingStudentStatistics(studentStatistics){

  let tableStatistics = document.getElementById('Statistics');
  tableStatistics.innerHTML = ''
  
  for(let i = 0; i < studentStatistics.length; ++i){
  let tr = document.createElement('tr');
  tableStatistics.appendChild(tr);

  let tdCourse = document.createElement('td');
  tr.appendChild(tdCourse);
  tdCourse.innerHTML = studentStatistics[i].course;

  let tdEstimate = document.createElement('td');
  tr.appendChild(tdEstimate);
  tdEstimate.innerHTML = studentStatistics[i].average_rating;

  let inActivStudent = document.createElement('td');
  tr.appendChild(inActivStudent);
  inActivStudent.innerHTML = studentStatistics[i].no_active;

  if (i == studentStatistics.length){
    let trSumNo_active = document.createElement('td');
    tr.appendChild(trSumNo_active);
    trSumNo_active.innerHTML = studentStatistics.sumNo_active;
  };
};

};
DisplayingStudentStatistics(studentStatistics);