/**Задан двумерный массив - объединить каждый внутренний массив с верхнем массивом - только по уникальным значениям.
Например [1,2,4[8,4,12,],[13,29,11],[0,5,3,11],5,6,7,[3,8,21],3],
получаем в результате: [1,2,4,8,12,13,29,11,0,5,3,11,6,7,21] */
function converting_2D_Array_in_1D_with_unique_values(array){
    function onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    };
    let result = [];
    for (let i in array){
        for(let u in array[i]){
            result.push(array[i][u])
        }; 
    };
    return console.log (result.filter(onlyUnique));
};

let array = [[1,2,3],
            [2,3,4,],
            [3,4,5,6]]

converting_2D_Array_in_1D_with_unique_values(array)
