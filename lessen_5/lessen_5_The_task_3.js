/**Задан массив объектов студентов вида [{name: “Ivan”, estimate: 4, course: 1, active: true},
 * {name: “Ivan”, estimate: 3, course: 1, active: true},{name: “Ivan”, estimate: 2, course: 4, active: false},
 * {name: “Ivan”, estimate: 5, course: 2, active: true}] - заполнить его более большим количеством студентов.
 * Написать функцию которая возвращает: среднюю оценку студентов в разрезе каждого курса: 
 * {1: 3.2, 2: 3.5, 3: 4.5, 4: 3, 5: 4.5} с учетом только тех студентов которые активны. 
 * Посчитать количество неактивных студентов в разрезе каждого курса и общее количество неактивных студентов.
 */

  let student = [
    {name: 'Ivan', estimate: 4, course: 1, active: true},
    {name: 'luda', estimate: 3, course: 1, active: true},
    {name: 'inga', estimate: 2, course: 4, active: false},
    {name: 'Azizbek', estimate: 4, course: 5, active: true},
    {name: 'Alibec', estimate: 4, course: 4, active: true},
    {name: 'Taras', estimate: 3, course: 2, active: true},
    {name: 'Adolf', estimate: 5, course: 3, active: true},
    {name: 'Joseph', estimate: 4, course: 5, active: true},
    {name: 'Winston', estimate: 2, course: 3, active: false},
    {name: 'Ilya', estimate: 4, course: 1, active: true},
    {name: 'Sasha', estimate: 2, course: 5, active: false},
    {name: 'Anita', estimate: 4, course: 2, active: true},
    {name: 'Diana', estimate: 1, course: 2, active: false},
    {name: 'Joe', estimate: 4, course: 4, active: true},
    {name: 'Donald', estimate: 5, course: 1, active: true}
  ];

function AverageGradePerCourse(student){
    let sumEstimate_1course = 0;
    let sumEstimate_2course = 0;
    let sumEstimate_3course = 0;
    let sumEstimate_4course = 0;
    let sumEstimate_5course = 0;
    let sumStudent_1course = 0;
    let sumStudent_2course = 0;
    let sumStudent_3course = 0;
    let sumStudent_4course = 0;
    let sumStudent_5course = 0;

    let inActivStudent_1course = 0;
    let inActivStudent_2course = 0;
    let inActivStudent_3course = 0;
    let inActivStudent_4course = 0;
    let inActivStudent_5course = 0;
    let sumInActivStudent = 0;
     
     for (let i = 0; i < student.length; ++i){
        if (student[i].course === 1 && student[i].active === true){
          sumEstimate_1course += student[i].estimate, ++sumStudent_1course 
        }
        else if (student[i].course === 1 && student[i].active === false){
          ++inActivStudent_1course, ++sumInActivStudent 
        }
        else if (student[i].course === 2 && student[i].active === true){
          sumEstimate_2course += student[i].estimate, ++sumStudent_2course 
        }
        else if (student[i].course === 2 && student[i].active === false){
          ++inActivStudent_2course, ++sumInActivStudent 
        }
        else if (student[i].course === 3 && student[i].active === true){
          sumEstimate_3course += student[i].estimate, ++sumStudent_3course 
        }
        else if (student[i].course === 3 && student[i].active === false){
          ++inActivStudent_3course, ++sumInActivStudent 
        }
        else if (student[i].course === 4 && student[i].active === true){
          sumEstimate_4course += student[i].estimate, ++sumStudent_4course 
        }
        else if (student[i].course === 4 && student[i].active === false){
          ++inActivStudent_4course, ++sumInActivStudent 
        }
        else if (student[i].course === 5 && student[i].active === true){
          sumEstimate_5course += student[i].estimate, ++sumStudent_5course 
        }
        else if (student[i].course === 5 && student[i].active === false){
          ++inActivStudent_5course, ++sumInActivStudent 
        };
        
      };
      let course_1 = sumEstimate_1course / sumStudent_1course;
      let course_2 = sumEstimate_2course / sumStudent_2course;
      let course_3 = sumEstimate_3course / sumStudent_3course;
      let course_4 = sumEstimate_4course / sumStudent_4course;
      let course_5 = sumEstimate_5course / sumStudent_5course;
     return console.log('средний бал первого курса: '+course_1,' средний бал второго курса: '+course_2,
     ' средний бал третего курса: '+course_3,' средний бал четвертого курса: '+course_4,
     ' средний бал пятого курса: '+course_5,' количество не активных студенто на первом курсе: '+inActivStudent_1course,
     ', на втором: '+inActivStudent_2course +', на третьем: '+inActivStudent_3course+', на четвертом: '+inActivStudent_4course +
      ', на пятом: '+inActivStudent_5course + ' и всего: ' + sumInActivStudent);
};

AverageGradePerCourse(student)