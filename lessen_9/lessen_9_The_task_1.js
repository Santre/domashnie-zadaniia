/**Написать ajax запрос получения данных по студентам из сервера (сервис будет предоставлен) */

let request = new XMLHttpRequest();
request.open("GET",'https://evgeniychvertkov.com/api/student/', true);
request.setRequestHeader("Content-type", "application/json; charset=utf-8");
request.setRequestHeader("X-Authorization-Token", "735121a3-34ac-11eb-a483-f1c3feb07438");

request.send();

request.onreadystatechange = function() {
    if (request.readyState != 4) return;
    if (request.status == 200) {
        let response = JSON.parse(request.responseText);
        let student = response.students;

        let table = document.querySelector('table tbody');
        table.innerHTML = ''

        for(let i = 0; i < student.length; ++i){
            let tr = document.createElement('tr');
            table.appendChild(tr);
        
            let tdDate = document.createElement('td');
            tr.appendChild(tdDate);
            tdDate.innerHTML = student[i].date;
            
            let tdName = document.createElement('td');
            tr.appendChild(tdName);
            tdName.innerHTML = student[i].first_name;
        
            let tdCourse = document.createElement('td');
            tr.appendChild(tdCourse);
            tdCourse.innerHTML = student[i].course;
        
            let tdEstimate = document.createElement('td');
            tr.appendChild(tdEstimate);
            tdEstimate.innerHTML = student[i].estimate;
        
            let tdEmail = document.createElement('td');
            tr.appendChild(tdEmail);
            tdEmail.innerHTML = student[i].email;
        
            let tdDelete = document.createElement('td');
            tdDelete.innerHTML = '<button class="idTable">Удалить</button>';
            tr.appendChild(tdDelete);
        
          };
    };
};
