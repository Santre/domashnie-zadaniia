/**Написать ajax запрос получения данных по студентам из сервера (сервис будет предоставлен)
Выводить данные полученные от сервера соответственно логике описанной в предыдущих занятиях предыдущих уроках
Аналогично для удаления, добавления и изменения статуса студентов написать ajax-запросы
Проверять если сервис доступный для работы со студентами работать с ним, если нет - то работать с localstorage как было реализовано ранее
 */
function Student (){
    this.students = [];
}

Student.prototype.render = function(){
    let tbody = document.querySelector(".students tbody");
    tbody.innerHTML = "";

    let self = this;

   for(let i = 0; i < this.students.length; i++){
     let tr = document.createElement("TR");

     let td = document.createElement("TD");
     td.innerHTML = this.students[i].date;
     tr.appendChild(td);

     td = document.createElement("TD");
     td.addEventListener('click',
      (function(index){
        return function(event){
          let inputNam = document.querySelectorAll('.neme');

          for (let u = 0; u <inputNam.length; ++u){
            let value = inputNam[u].value;
            let cell = inputNam[u].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputName = document.createElement('input');
          inputName.type = 'text';
          inputName.className = 'neme';
          inputName.value = self.students[index].first_name;
          td.innerHTML = '';
          td.appendChild(inputName);

          inputName.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              self.students[index].first_name = event.target.value;
              self.update(self.students[index]);
            };
          });
          inputName.focus();
        }  
    })(i));
     td.innerHTML = this.students[i].first_name;
     tr.appendChild(td);

     td = document.createElement("TD");
     td.addEventListener('click',
      (function(index){
        return function(event){
          let inputСours = document.querySelectorAll('.neme');

          for (let u = 0; u <inputСours.length; ++u){
            let value = inputСours[u].value;
            let cell = inputСours[u].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputСourse = document.createElement('input');
          inputСourse.type = 'text';
          inputСourse.className = 'neme';
          inputСourse.value = self.students[index].course;
          td.innerHTML = '';
          td.appendChild(inputСourse);

          inputСourse.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let  courseEstimate = /[1-5]/;
              let value = event.target.value;
              if (courseEstimate.test(value)){
                self.students[index].course = event.target.value;
                self.update(self.students[index]);
              }else{alert ('Данные введены неверно')};
              
            };
          });
          inputСourse.focus();
        }  
    })(i));
     td.innerHTML = this.students[i].course;
     tr.appendChild(td);

     td = document.createElement("TD");
     td.addEventListener('click',
      (function(index){
        return function(event){
          let inputEstimate = document.querySelectorAll('.neme');

          for (let u = 0; u <inputEstimate.length; ++u){
            let value = inputEstimate[u].value;
            let cell = inputEstimate[u].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputEstimat = document.createElement('input');
          inputEstimat.type = 'text';
          inputEstimat.className = 'neme';
          inputEstimat.value = self.students[index].estimate;
          td.innerHTML = '';
          td.appendChild(inputEstimat);

          inputEstimat.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let  courseEstimate = /[1-5]/;
              let value = event.target.value;
                if (courseEstimate.test(value)){
                  self.students[index].estimate = event.target.value;
                  self.update(self.students[index]);
                }else {alert ('Данные введены неверно')};
            };
          });
          inputEstimat.focus();
        }  
    })(i));
     td.innerHTML = this.students[i].estimate;
     tr.appendChild(td);

     td = document.createElement("TD");
     td.innerHTML = this.students[i].Email;
     tr.appendChild(td);
     
     if(this.students[i].is_active){
        let td = document.createElement("TD");
        td.innerHTML = 'Активный';
        tr.appendChild(td);
     }else{
        let td = document.createElement("TD");
        td.innerHTML = 'НЕ активный';
        tr.appendChild(td);
     }

     td = document.createElement("TD");
     td.innerHTML = '<button class="idTable">Удалить</button>';
     td.addEventListener("click", (function(index) {
         return function(event){
            self.removeOnServer(index);
            self.render();
        };
    })(i));
     tr.appendChild(td);

     tbody.appendChild(tr);

    let tdBackgroundColor = document.querySelectorAll('td');
    for (let u=0; u<tdBackgroundColor.length; u++) {
        if(tdBackgroundColor[u].textContent  <= 3 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FF0000"}
        else if (tdBackgroundColor[u].textContent  == 4 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FFFF00"}
        else if (tdBackgroundColor[u].textContent  == 5 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#008000"}
    };
   };
};

Student.prototype.remove = function(index){
    this.students.splice(index, 1);
};






let request = new XMLHttpRequest();
request.open("GET",'https://evgeniychvertkov.com/api/student/', true);
request.setRequestHeader("Content-type", "application/json; charset=utf-8");
request.setRequestHeader("X-Authorization-Token", "735121a3-34ac-11eb-a483-f1c3feb07438");

request.send();

request.onreadystatechange = function() {
  if (request.readyState != 4) return;
  if (request.status == 200) {
      let response = JSON.parse(request.responseText);
      let student = response.students;
      // return  console.log (student);
      let Statistics = [];
  function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
  };

  let anchor = [];
  for (let i = 0; i < student.length; i++){
    anchor.push(
      student[i].course
    );
  };
  let anchor2 = anchor.filter(onlyUnique);

  anchor2.sort();
  for (let u = 0; u < anchor2.length; u++) {
    Statistics.push(
    {course: anchor2[u], average_rating: 0, no_active: 0, sumStudent:0 }
    );
  };

  for (let y = 0; y < student.length; y++) {
    for (let t = 0; t < Statistics.length; t++) {
      if (Statistics[t].course == student[y].course){
        Statistics[t].average_rating += student[y].estimate;
        Statistics[t].sumStudent += 1;
      };
      if (Statistics[t].course == student[y].course && student[y].is_active == Boolean(false)){
        Statistics[t].no_active += 1; 
      }
    };
  };
  for (let r = 0; r < Statistics.length; r++) {
    Statistics[r].average_rating = Math.trunc(Statistics[r].average_rating / Statistics[r].sumStudent * 100) / 100;
  };
  
  let sumNo_active = 0;
  for (let e = 0; e < Statistics.length; e++) {
    sumNo_active += Statistics[e].no_active;
  };
  Statistics.push(
    {sumNo_active: sumNo_active}
    );
    
    let tableStatistics = document.getElementById('Statistics');
    tableStatistics.innerHTML = ''
    
    for(let i = 0; i < Statistics.length; ++i){
    let tr = document.createElement('tr');
    tableStatistics.appendChild(tr);

    let tdCourse = document.createElement('td');
    tr.appendChild(tdCourse);
    tdCourse.innerHTML = Statistics[i].course;

    let tdEstimate = document.createElement('td');
    tr.appendChild(tdEstimate);
    tdEstimate.innerHTML = Statistics[i].average_rating;

    let inActivStudent = document.createElement('td');
    tr.appendChild(inActivStudent);
    inActivStudent.innerHTML = Statistics[i].no_active;

    let tdBackgroundColor = document.querySelectorAll('td');
    for (let u=0; u<tdBackgroundColor.length; u++) {
        if(tdBackgroundColor[u].textContent  <= 3 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FF0000"}
        else if (tdBackgroundColor[u].textContent  == 4 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FFFF00"}
        else if (tdBackgroundColor[u].textContent  == 5 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#008000"}
    };
  };
  };
  
};


Student.prototype.removeOnServer = function(index){
    let self = this;
    function callback(response){
        if(!response.is_error){
            self.remove(index);
            self.render();
        }
    }

    this.send("DELETE", 
    'https://evgeniychvertkov.com/api/student/' + this.students[index].id + '/', 
    callback);
};

Student.prototype.send = function(method, url, callback, obj){
    let self = this;

    let data = obj || false;

    var xhr = new XMLHttpRequest();

    xhr.open(method, url, true);

    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhr.setRequestHeader("X-Authorization-Token", "735121a3-34ac-11eb-a483-f1c3feb07438");

    if(data){
        xhr.send(JSON.stringify(data));
    }else{
        xhr.send();
    }
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status == 200) {
            let response = JSON.parse(xhr.responseText);
            callback(response);
        }
    }
};

Student.prototype.getAll = function(){
    let self = this;
    function callback(response){
        if(!response.is_error){
            self.students = response.students;
            self.render();
        }
    }
    this.send("GET", 
   'https://evgeniychvertkov.com/api/student/', 
    callback);
};

Student.prototype.add = function(student){
    let self = this;
    function callback(response){
        if(!response.is_error){
            self.students.push(response.student);
            self.render();
        }
    }
    this.send("POST", 
   'https://evgeniychvertkov.com/api/student/', 
    callback, student);
};


Student.prototype.update = function(student){
  let self = this;
  function callback(response){
      if(!response.is_error){
          self.render();
      }
  }
  this.send("PUT", 
 'https://evgeniychvertkov.com/api/student/', 
  callback, student);
};

window.onload = function(){
    let student = new Student();
    student.getAll();

    let button = document.getElementById("add");

    button.addEventListener("click", function(event){
        let data = new FormData(event.target.closest("form"));
        student.add({
            first_name: data.get("first_name"),
            course: data.get("course"),
            estimate: data.get("estimate"),
            email: data.get("email"),
            is_active: data.get("is_active") !== null
        });
    });
};