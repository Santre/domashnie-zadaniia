/**Написать функцию, которая транспонирует матрицу */
function lessen_3_The_task_3(a){
    var b = [];
    for (let i in a){
        b[i]= [];
        for(let u in a){
            b[i][u] = a[u][i];
        };
    };
    return b;
};
var c = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
];
console.log(lessen_3_The_task_3(c));

/**Написать функцию, которая сумирует две матрицы */

function lessen_3_The_task_4(a, b){
    var c =[];
    for (let i in a){
        c[i] = []
        for(let u in b){
            c[i][u] = (a[i][u] + b[i][u])}
        };
        return c;
    };
var v = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
];
var d = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
];
 console.log(lessen_3_The_task_4(v, d));

 /**Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки) */
let array = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
    ];

    function indexLine(a){
        let b = [];
        let c = 0;
        for( let i in a){
            b[i] = 0;
            for (let u in a){
              b[i] += a[i][u];
              if (c < b[i]){c = b[i]};
            };     
        };
        return console.log(b.indexOf(c));
    };
    indexLine(array);

let array2 = [
    [11, 12, 13],
    [21, 22, 23],
    [31, 32, 33]
];
    function indexCool(a){
        let b = a[0];
        let c = 0;
        for( let i in a){
          for (let u in a){
            b[u] += a[i][u];
            if(c < b[u]){c = b[u]};
          };
        };
        return console.log(b.indexOf(c));
    };
indexCool(array2);