/**-Добавить еще одно свойство для студента - email адрес
* Вывести mail адрес на ряду в списке студентов
* Так же организовать возможность изменения по аналогии с именем
* Написать регулярное выражение проверки введенных данных email
* Написать валидацию проверку ввода данных - курс это любое целое число от 1 до 5, email - с помощью регулярки, 
  имя может содержать только буквы не более 15 символов
* Если при вводе в любую из форм введены невалидные данные - сообщать об этом в окне с ошибкой
* Изменять данные физически тогда и только тогда, когда будут введены корректные
* Добавить еще одно свойство: date - дата создания студента, брать текущее время клиента и записывать наряду с каждым студентом,
   выводить дату в списке со студентами - ее нельзя изменить.
* Добавить в статистику еще одно вычисление:  количество студентов которые были добавлены за последний час.
* Все данные хранить в localStorage касательно студентов
* При открытии страницы выбирать из localstorage и показывать список студентов и статистику соответственно
* Если localstorage не имеет никаких данных в себе, показывать что ничего нет при добавлении из формы нового студента - данные должны попасть и сохраниться в localstorage, при следующем открытии страницы - уже подтянуться сохраненные данные

 */

// var student = [
//     {name: 'Ivan', estimate: 4, course: 1, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'luda', estimate: 3, course: 1, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'inga', estimate: 2, course: 4, active: 0, email: 'random@email', date: '30.11.2020'},
//     {name: 'Azizbek', estimate: 4, course: 5, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'Alibec', estimate: 4, course: 4, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'Taras', estimate: 3, course: 2, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'Adolf', estimate: 5, course: 3, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'Joseph', estimate: 4, course: 5, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'Winston', estimate: 2, course: 3, active: 0, email: 'random@email', date: '30.11.2020'},
//     {name: 'Ilya', estimate: 4, course: 1, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'Sasha', estimate: 2, course: 5, active: 0, email:'random@email', date: '30.11.2020'},
//     {name: 'Anita', estimate: 4, course: 2, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'Diana', estimate: 1, course: 2, active: 0, email:'random@email', date: '30.11.2020'},
//     {name: 'Joe', estimate: 4, course: 4, active: 1, email: 'random@email', date: '30.11.2020'},
//     {name: 'Donald', estimate: 5, course: 1, active: 1, email: 'random@email', date: '30.11.2020'}
//   ];
//   localStorage.setItem('storedStudent', JSON.stringify(student));

  function FormationOf_a_ContingentOfStudents(student){
    student = JSON.parse(student);
    if (student === []){
      alert('Данных нет')
    };
    let table = document.querySelector('table tbody');
    table.innerHTML = ''
    for(let i = 0; i < student.length; ++i){
    let tr = document.createElement('tr');
    table.appendChild(tr);

    let tdDate = document.createElement('td');
    tr.appendChild(tdDate);
    tdDate.innerHTML = student[i].date;
    
    let tdName = document.createElement('td');
    tdName.addEventListener('click',
      (function(index){
        return function(event){
          let inputNam = document.querySelectorAll('.neme');

          for (let u = 0; u <inputNam.length; ++u){
            let value = inputNam[u].value;
            let cell = inputNam[u].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputName = document.createElement('input');
          inputName.type = 'text';
          inputName.className = 'neme';
          inputName.value = student[index].name;
          tdName.innerHTML = '';
          tdName.appendChild(inputName);

          inputName.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let value = event.target.value
              let cell = event.target.closest('td');
              cell.innerHTML = '<span>' + value + '</span>';
              student[index].name = event.target.value;
              localStorage.setItem('storedStudent', JSON.stringify(student));
              // self.update(self.student[index]);
            };
          });
          inputName.focus();
        }  
    })(i));
    tr.appendChild(tdName);
    tdName.innerHTML = student[i].name;

    let tdCourse = document.createElement('td');
    tdCourse.addEventListener('click',
      (function(index){
        return function(event){
          let inputCourse = document.querySelectorAll('.course');

          for (let y = 0; y <inputCourse.length; ++y){
            let value = inputCourse[y].value;
            let cell = inputCourse[y].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputCours = document.createElement('input');
          inputCours.type = 'number';
          inputCours.className = 'course';
          inputCours.value = student[index].course;
          tdCourse.innerHTML = '';
          tdCourse.appendChild(inputCours);

          inputCours.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let  courseEstimate = /[1-5]/;
              let value = event.target.value;
                if (courseEstimate.test(value)){
                let cell = event.target.closest('td');
                cell.innerHTML = '<span>' + value + '</span>';
                student[index].course = event.target.value;
                localStorage.setItem('storedStudent', JSON.stringify(student));
                // self.update(self.student[index]);
                DisplayingStudentStatistics(AverageGradePerCourse(student));
              }else {alert ('Данные введены неверно')};
            };
          });
          inputCours.focus();
        }  
    })(i));
    tr.appendChild(tdCourse);
    tdCourse.innerHTML = student[i].course;

    let tdEstimate = document.createElement('td');
    tdEstimate.addEventListener('click',
      (function(index){
        return function(event){
          let inputEstimate = document.querySelectorAll('.estimate');

          for (let t = 0; t <inputEstimate.length; ++t){
            let value = inputEstimate[t].value;
            let cell = inputEstimate[t].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputEstimat = document.createElement('input');
          inputEstimat.type = 'number';
          inputEstimat.className = 'estimate';
          inputEstimat.value = student[index].estimate;
          tdEstimate.innerHTML = '';
          tdEstimate.appendChild(inputEstimat);

          inputEstimat.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let  courseEstimate = /[1-5]/;
              let value = event.target.value;
              if (courseEstimate.test(value)){
                let cell = event.target.closest('td');
                cell.innerHTML = '<span>' + value + '</span>';
                student[index].estimate = event.target.value;
                localStorage.setItem('storedStudent', JSON.stringify(student));
                // self.update(self.student[index]);
                DisplayingStudentStatistics(AverageGradePerCourse(student));
              }else {alert ('Данные введены неверно')};
            };
          });
          inputEstimat.focus();
        }  
    })(i));
    tr.appendChild(tdEstimate);
    tdEstimate.innerHTML = student[i].estimate;

    let tdEmail = document.createElement('td');
    tdEmail.addEventListener('click',
      (function(index){
        return function(event){
          let inputEmail = document.querySelectorAll('.email');

          for (let t = 0; t <inputEmail.length; ++t){
            let value = inputEmail[t].value;
            let cell = inputEmail[t].closest('td');
            cell.innerHTML = '<span>' + value + '</span>';
          };
          let inputEmaill = document.createElement('input');
          inputEmaill.type = 'email';
          inputEmaill.className = 'email';
          inputEmaill.value = student[index].email;
          tdEmail.innerHTML = '';
          tdEmail.appendChild(inputEmaill);

          inputEmaill.addEventListener('keydown', function(event){
            if (event.keyCode === 13) {
              let  email = /^[^\s@]{1,15}@[^\s@]+\.[^\s@]+$/;
              let value = event.target.value;
              if (email.test(value)){
                let cell = event.target.closest('td');
                cell.innerHTML = '<span>' + value + '</span>';
                student[index].email = event.target.value;
                localStorage.setItem('storedStudent', JSON.stringify(student));
                // self.update(self.student[index]);
            }else {alert ('Данные введены неверно')};
            };
          });
          inputEmaill.focus();
        }  
    })(i));
    tr.appendChild(tdEmail);
    tdEmail.innerHTML = student[i].email;

    let tdDelete = document.createElement('td');
    tdDelete.innerHTML = '<button class="idTable">Удалить</button>';
    tr.appendChild(tdDelete);

  };
  };
  FormationOf_a_ContingentOfStudents(localStorage.getItem('storedStudent',));

  function AverageGradePerCourse(student){
    student = JSON.parse(student);
    let Statistics = [];
    function onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
    };

    let anchor = [];
    for (let i = 0; i < student.length; i++){
      anchor.push(
        student[i].course
      );
    };
    let anchor2 = anchor.filter(onlyUnique);

    anchor2.sort();
    for (let u = 0; u < anchor2.length; u++) {
      Statistics.push(
      {course: anchor2[u], average_rating: 0, no_active: 0, sumStudent:0 }
      );
    };

    for (let y = 0; y < student.length; y++) {
      for (let t = 0; t < Statistics.length; t++) {
        if (Statistics[t].course == student[y].course){
          Statistics[t].average_rating += student[y].estimate;
          Statistics[t].sumStudent += 1;
        };
        if (Statistics[t].course == student[y].course && student[y].active == 0){
          Statistics[t].no_active += 1; 
        };
      };
    };
    for (let r = 0; r < Statistics.length; r++) {
      Statistics[r].average_rating = Math.trunc(Statistics[r].average_rating / Statistics[r].sumStudent * 100) / 100;
    };
    
    let sumNo_active = 0;
    for (let e = 0; e < Statistics.length; e++) {
      sumNo_active += Statistics[e].no_active;
    };
    Statistics.push(
      {sumNo_active: sumNo_active}
      );
      return Statistics
};

let studentStatistics = AverageGradePerCourse(localStorage.getItem('storedStudent',));
console.log (studentStatistics);

function DisplayingStudentStatistics(studentStatistics){

    let tableStatistics = document.getElementById('Statistics');
    tableStatistics.innerHTML = ''
    
    for(let i = 0; i < studentStatistics.length; ++i){
    let tr = document.createElement('tr');
    tableStatistics.appendChild(tr);

    let tdCourse = document.createElement('td');
    tr.appendChild(tdCourse);
    tdCourse.innerHTML = studentStatistics[i].course;

    let tdEstimate = document.createElement('td');
    tr.appendChild(tdEstimate);
    tdEstimate.innerHTML = studentStatistics[i].average_rating;

    let inActivStudent = document.createElement('td');
    tr.appendChild(inActivStudent);
    inActivStudent.innerHTML = studentStatistics[i].no_active;

    let tdBackgroundColor = document.querySelectorAll('td');
    for (let u=0; u<tdBackgroundColor.length; u++) {
        let num = +tdBackgroundColor[u].textContent;
        if(num  <= 3 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FF0000"}
        else if (num  == 4 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#FFFF00"}
        else if (num  == 5 ) {tdBackgroundColor[u].parentNode.style.backgroundColor = "#008000"}
    };
  };
 
  };
  DisplayingStudentStatistics(studentStatistics);

let button = document.getElementById('newStudent1');
  button.addEventListener('click', function(event){
      let inputNewStudent = document.querySelector('input[name = "newStudent"]');
      let inputCourse = document.querySelector('input[name = "course"]');
      let inputEstimate = document.querySelector('input[name = "estimate"]');
      let selectActiveStudent = document.querySelector('select[name="active"]');
      let inpuEmail = document.querySelector('input[name = "email"]');
      let date =  new Date();
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year= date.getFullYear();
      let dataDayMonthYear = day + '.' +  month + '.' + year;
      let student = localStorage.getItem('storedStudent',);
      student = JSON.parse(student);
      
        let  courseEstimate = /[1-5]/;
        let  email = /^[^\s@]{1,15}@[^\s@]+\.[^\s@]+$/;
        if (courseEstimate.test(courseEstimate) && courseEstimate.test(inputEstimate.value) && email.test(inpuEmail.value)){
            student.push({
                name: inputNewStudent.value, 
                estimate: parseInt(inputEstimate.value), 
                course: parseInt(inputCourse.value),
                active: parseInt(selectActiveStudent.value),
                email:  inpuEmail.value,
                date:  dataDayMonthYear
            });
        }
        else{
            alert( 'Данные введены неверно')
        };
        localStorage.setItem('storedStudent', JSON.stringify(student));
      FormationOf_a_ContingentOfStudents(localStorage.getItem('storedStudent',));
      DisplayingStudentStatistics(AverageGradePerCourse(localStorage.getItem('storedStudent',)));
      ;

      
  });
  

  [].forEach.call(document.querySelectorAll('button.idTable'), function(el){
    el.addEventListener('click', function () {
      let tr= this.parentNode.parentNode;
      var index = tr.rowIndex - 1;
      tableBody.deleteRow(index);
      let student = localStorage.getItem('storedStudent',);
      student = JSON.parse(student);
      student.splice(index, 1);
      if (student[0] == null )(alert('Студенты не найдены'));
      localStorage.setItem('storedStudent', JSON.stringify(student));
      DisplayingStudentStatistics(AverageGradePerCourse(localStorage.getItem('storedStudent',)));
    });
  });














































