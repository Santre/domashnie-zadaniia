/**Посчитать количество тегов “p” на странице которые имеют класс “phrase” - вывести их содержимое */
let div = document.querySelector('div');
let P_ClassPhrase = div.getElementsByClassName('phrase');
div.appendChild(document.createElement('p')).innerHTML ='количество классов “phrase” на странице: ' 
+ P_ClassPhrase.length + ', ниже приведено содержание параграфов с данным классом';
let ol = document.createElement('ol');
div.appendChild(ol);

for(let i =0; i< P_ClassPhrase.length; ++i){
    ol.appendChild(document.createElement('li')).innerHTML = P_ClassPhrase[i].outerText;
 };
