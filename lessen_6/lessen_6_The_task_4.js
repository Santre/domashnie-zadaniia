/**Вывести статистику средних оценок в разрезе курса и статистику по количеству неактивных студентов 
 * в разрезе каждого курса и общее количество неактивных студентов */
var student = [
    {name: 'Ivan', estimate: 4, course: 1, active: true},
    {name: 'luda', estimate: 3, course: 1, active: true},
    {name: 'inga', estimate: 2, course: 4, active: false},
    {name: 'Azizbek', estimate: 4, course: 5, active: true},
    {name: 'Alibec', estimate: 4, course: 4, active: true},
    {name: 'Taras', estimate: 3, course: 2, active: true},
    {name: 'Adolf', estimate: 5, course: 3, active: true},
    {name: 'Joseph', estimate: 4, course: 5, active: true},
    {name: 'Winston', estimate: 2, course: 3, active: false},
    {name: 'Ilya', estimate: 4, course: 1, active: true},
    {name: 'Sasha', estimate: 2, course: 5, active: false},
    {name: 'Anita', estimate: 4, course: 2, active: true},
    {name: 'Diana', estimate: 1, course: 2, active: false},
    {name: 'Joe', estimate: 4, course: 4, active: true},
    {name: 'Donald', estimate: 5, course: 1, active: true}
  ];

  function AverageGradePerCourse(student){
    let sumEstimate_1course = 0;
    let sumEstimate_2course = 0;
    let sumEstimate_3course = 0;
    let sumEstimate_4course = 0;
    let sumEstimate_5course = 0;
    let sumStudent_1course = 0;
    let sumStudent_2course = 0;
    let sumStudent_3course = 0;
    let sumStudent_4course = 0;
    let sumStudent_5course = 0;

    let inActivStudent_1course = 0;
    let inActivStudent_2course = 0;
    let inActivStudent_3course = 0;
    let inActivStudent_4course = 0;
    let inActivStudent_5course = 0;
    let sumInActivStudent = 0;
     
     for (let i = 0; i < student.length; ++i){
        if (student[i].course === 1 && student[i].active === true){
          sumEstimate_1course += student[i].estimate, ++sumStudent_1course 
        }
        else if (student[i].course === 1 && student[i].active === false){
          ++inActivStudent_1course, ++sumInActivStudent 
        }
        else if (student[i].course === 2 && student[i].active === true){
          sumEstimate_2course += student[i].estimate, ++sumStudent_2course 
        }
        else if (student[i].course === 2 && student[i].active === false){
          ++inActivStudent_2course, ++sumInActivStudent 
        }
        else if (student[i].course === 3 && student[i].active === true){
          sumEstimate_3course += student[i].estimate, ++sumStudent_3course 
        }
        else if (student[i].course === 3 && student[i].active === false){
          ++inActivStudent_3course, ++sumInActivStudent 
        }
        else if (student[i].course === 4 && student[i].active === true){
          sumEstimate_4course += student[i].estimate, ++sumStudent_4course 
        }
        else if (student[i].course === 4 && student[i].active === false){
          ++inActivStudent_4course, ++sumInActivStudent 
        }
        else if (student[i].course === 5 && student[i].active === true){
          sumEstimate_5course += student[i].estimate, ++sumStudent_5course 
        }
        else if (student[i].course === 5 && student[i].active === false){
          ++inActivStudent_5course, ++sumInActivStudent 
        };
        
      };
      let course_1 = sumEstimate_1course / sumStudent_1course;
      let course_2 = sumEstimate_2course / sumStudent_2course;
      let course_3 = sumEstimate_3course / sumStudent_3course;
      let course_4 = sumEstimate_4course / sumStudent_4course;
      let course_5 = sumEstimate_5course / sumStudent_5course;
      let studentStatistics = [
        { AverageScore: course_1, course: 1, inActivStudent: inActivStudent_1course},
        { AverageScore: course_2, course: 2, inActivStudent: inActivStudent_2course },
        { AverageScore: course_3, course: 3, inActivStudent: inActivStudent_3course },
        { AverageScore: course_4, course: 4, inActivStudent: inActivStudent_4course },
        { AverageScore: course_5, course: 5, inActivStudent: inActivStudent_5course },
        // {sumInActivStudent: sumInActivStudent}
    ];
     return studentStatistics;
};

let studentStatistics = AverageGradePerCourse(student);
console.log (studentStatistics);

function DisplayingStudentStatistics(studentStatistics){

    let tableStatistics = document.getElementById('Statistics');
    
    
    for(let i = 0; i < studentStatistics.length; ++i){
    let tr = document.createElement('tr');
    tableStatistics.appendChild(tr);

    let tdCourse = document.createElement('td');
    tr.appendChild(tdCourse);
    tdCourse.innerHTML = studentStatistics[i].course;

    let tdEstimate = document.createElement('td');
    tr.appendChild(tdEstimate);
    tdEstimate.innerHTML = studentStatistics[i].AverageScore;

    let inActivStudent = document.createElement('td');
    tr.appendChild(inActivStudent);
    inActivStudent.innerHTML = studentStatistics[i].inActivStudent;
  };
 
  };
  DisplayingStudentStatistics(studentStatistics);