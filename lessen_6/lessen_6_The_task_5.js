/** Добавить текстовое поле ввода - ввод имени студента, поле ввода для курса, оценки и checkbox для 
активности студента, по нажатии на кнопку “Добавить” - студент новый добавляется в список студентов*/

var student = [
    {name: 'Ivan', estimate: 4, course: 1, active: true},
    {name: 'luda', estimate: 3, course: 1, active: true},
    {name: 'inga', estimate: 2, course: 4, active: false},
    {name: 'Azizbek', estimate: 4, course: 5, active: true},
    {name: 'Alibec', estimate: 4, course: 4, active: true},
    {name: 'Taras', estimate: 3, course: 2, active: true},
    {name: 'Adolf', estimate: 5, course: 3, active: true},
    {name: 'Joseph', estimate: 4, course: 5, active: true},
    {name: 'Winston', estimate: 2, course: 3, active: false},
    {name: 'Ilya', estimate: 4, course: 1, active: true},
    {name: 'Sasha', estimate: 2, course: 5, active: false},
    {name: 'Anita', estimate: 4, course: 2, active: true},
    {name: 'Diana', estimate: 1, course: 2, active: false},
    {name: 'Joe', estimate: 4, course: 4, active: true},
    {name: 'Donald', estimate: 5, course: 1, active: true}
  ];
  
  
  function FormationOf_a_ContingentOfStudents(student){
    let table = document.querySelector('table tbody');
    table.innerHTML = ''
    for(let i = 0; i < student.length; ++i){
    let tr = document.createElement('tr');
    table.appendChild(tr);

    let tdName = document.createElement('td');
    tr.appendChild(tdName);
    tdName.innerHTML = student[i].name;

    let tdCourse = document.createElement('td');
    tr.appendChild(tdCourse);
    tdCourse.innerHTML = student[i].course;

    let tdEstimate = document.createElement('td');
    tr.appendChild(tdEstimate);
    tdEstimate.innerHTML = student[i].estimate;

    // let checkbox = document.createElement('td');
    // checkbox.innerHTML = '<input type="checkbox" name="active">';
    // tr.appendChild(checkbox);

  };
  };
  FormationOf_a_ContingentOfStudents(student);

let button = document.getElementById('newStudent1');
button.addEventListener('click', function(event){
    let inputNewStudent = document.querySelector('input[name = "newStudent"]');
    let inputCourse = document.querySelector('input[name = "course"]');
    let inputEstimate = document.querySelector('input[name = "estimate"]');
    // let checkboxActive = document.querySelector('input[name="active"]')
    student.push({
        name: inputNewStudent.value, 
        estimate: parseInt(inputEstimate.value), 
        course: parseInt(inputCourse.value),
        active: checkboxActive.value
    });
    FormationOf_a_ContingentOfStudents(student);
    console.dir(student)
});
