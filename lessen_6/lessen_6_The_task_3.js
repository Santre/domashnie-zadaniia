/**В задании из пятого урока, взять массив со студентами и вывести их на страницу согласно сверстанной 
 * HTML-структуре, рядом с каждым студентом вывести крестик - по нажатию на который студент будет удален 
 * (удаляется как со страницы, так и с объекта), если был удален последний студент написать текстовое 
 * сообщение (“Студенты не найдены”)
 */
var student = [
    {name: 'Ivan', estimate: 4, course: 1, active: true},
    {name: 'luda', estimate: 3, course: 1, active: true},
    {name: 'inga', estimate: 2, course: 4, active: false},
    {name: 'Azizbek', estimate: 4, course: 5, active: true},
    {name: 'Alibec', estimate: 4, course: 4, active: true},
    {name: 'Taras', estimate: 3, course: 2, active: true},
    {name: 'Adolf', estimate: 5, course: 3, active: true},
    {name: 'Joseph', estimate: 4, course: 5, active: true},
    {name: 'Winston', estimate: 2, course: 3, active: false},
    {name: 'Ilya', estimate: 4, course: 1, active: true},
    {name: 'Sasha', estimate: 2, course: 5, active: false},
    {name: 'Anita', estimate: 4, course: 2, active: true},
    {name: 'Diana', estimate: 1, course: 2, active: false},
    {name: 'Joe', estimate: 4, course: 4, active: true},
    {name: 'Donald', estimate: 5, course: 1, active: true}
  ];

  function FormationOf_a_ContingentOfStudents(student){
    let table = document.querySelector('table tbody');
    table.innerHTML = ''
    for(let i = 0; i < student.length; ++i){
    let tr = document.createElement('tr');
    table.appendChild(tr);

    let tdName = document.createElement('td');
    tr.appendChild(tdName);
    tdName.innerHTML = student[i].name;

    let tdCourse = document.createElement('td');
    tr.appendChild(tdCourse);
    tdCourse.innerHTML = student[i].course;

    let tdEstimate = document.createElement('td');
    tr.appendChild(tdEstimate);
    tdEstimate.innerHTML = student[i].estimate;

    let tdDelete = document.createElement('td');
    tdDelete.innerHTML = '<button class="idTable">Удалить</button>';
    tr.appendChild(tdDelete);

  };
  };
  FormationOf_a_ContingentOfStudents(student);

student.forEach.call(document.querySelectorAll('button.idTable'), function(el){
  el.addEventListener('click', function () {
    let tr= this.parentNode.parentNode;
    var index = tr.rowIndex - 1;
    tableBody.deleteRow(index);
    student.splice(index, 1);
    if (student[0] == null )(alert('Студенты не найдены'))
  })
});
